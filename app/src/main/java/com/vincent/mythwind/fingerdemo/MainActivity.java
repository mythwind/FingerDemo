
package com.vincent.mythwind.fingerdemo;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.Fingerprint;
import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 *
 * 通过 反射获取指纹列表
 *
 */
public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS = 0;

    private FingerprintManager fingerprintManager;
    private KeyguardManager mKeyguardManager;

    private CancellationSignal mCancellationSignal = new CancellationSignal();
    //回调方法
    private FingerprintManager.AuthenticationCallback mSelfCancelled = new FingerprintManager.AuthenticationCallback() {
        @Override
        public void onAuthenticationError(int errorCode, CharSequence errString) {
            //但多次指纹密码验证错误后，进入此方法；并且，不能短时间内调用指纹验证
            Toast.makeText(MainActivity.this, errString, Toast.LENGTH_SHORT).show();
            showAuthenticationScreen();
            Log.e(TAG,"onAuthenticationError:" + errorCode);
            Log.e(TAG, errString.toString());
        }

        @Override
        public void onAuthenticationHelp(int helpCode, CharSequence helpString) {

            Toast.makeText(MainActivity.this, helpString, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {

            Toast.makeText(MainActivity.this, "指纹识别成功", Toast.LENGTH_SHORT).show();

            FingerprintManager.CryptoObject obj = result.getCryptoObject();
            Log.e(TAG, "" + (obj == null ? "obj is null " : obj.getSignature()));


        }

        @Override
        public void onAuthenticationFailed() {
            Toast.makeText(MainActivity.this, "指纹识别失败", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        fingerprintManager = (FingerprintManager)getSystemService(Context.FINGERPRINT_SERVICE);
        mKeyguardManager = (KeyguardManager)getSystemService(Context.KEYGUARD_SERVICE);


        try {
            // fingerprintManager.
            Method m = fingerprintManager.getClass().getDeclaredMethod("getEnrolledFingerprints");
            m.setAccessible(true);
            List<Fingerprint> s = (List<Fingerprint>) m.invoke(fingerprintManager);
            for (Fingerprint fingerprint : s) {
                Log.e(TAG, fingerprint.getName() + "" );
                Log.e(TAG, fingerprint.getDeviceId() + "" );
                Log.e(TAG, fingerprint.getFingerId() + "" );
                Log.e(TAG, fingerprint.getGroupId() + "" );
            }

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        findViewById(R.id.btn_activity_main_finger).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFinger()) {
                    Toast.makeText(MainActivity.this, "请指纹识别", Toast.LENGTH_SHORT).show();
                    startListening(null);
                }
            }
        });


    }

    private boolean isFinger() {
        //android studio 上，没有这个会报错
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "没有指纹识别权限", Toast.LENGTH_SHORT).show();
            return false;
        }
        Log.e(TAG, "有指纹权限");
        //判断硬件是否支持指纹识别
        if (!fingerprintManager.isHardwareDetected()) {
            Toast.makeText(this, "没有指纹识别模块", Toast.LENGTH_SHORT).show();
            return false;
        }
        Log.e(TAG, "有指纹模块");
        //判断 是否开启锁屏密码

        if (!mKeyguardManager.isKeyguardSecure()) {
            Toast.makeText(this, "没有开启锁屏密码", Toast.LENGTH_SHORT).show();
            return false;
        }
        Log.e(TAG, "已开启锁屏密码");

        //判断是否有指纹录入
        if (!fingerprintManager.hasEnrolledFingerprints()) {
            Toast.makeText(this, "没有录入指纹", Toast.LENGTH_SHORT).show();
            return false;
        }
        Log.e(TAG, "已录入指纹");

        return true;

    }

    private void startListening(FingerprintManager.CryptoObject cryptoObject) {
        //android studio 上，没有这个会报错
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "没有指纹识别权限", Toast.LENGTH_SHORT).show();
            return;
        }
        fingerprintManager.authenticate(cryptoObject, mCancellationSignal, 0, mSelfCancelled, null);


    }
    private void showAuthenticationScreen() {
        Intent intent = mKeyguardManager.createConfirmDeviceCredentialIntent("finger", "测试指纹识别");
        if (intent != null) {
            startActivityForResult(intent, REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS) {
            // Challenge completed, proceed with using cipher
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "识别成功", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "识别失败", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
